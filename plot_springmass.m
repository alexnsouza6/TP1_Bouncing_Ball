load('saida.txt');
figure;
% hold on; 
for c=1:100 % size(saida,1),
   x = [saida(c,1),saida(c,3)];
   y = [saida(c,2),saida(c,4)];
   w = [saida(c,5),saida(c,7)];
   z = [saida(c,6),saida(c,8)];

   plot(saida(c,1:2:8), saida(c,2:2:8), 'o', 'MarkerSize', 20, 'LineWidth', 1);
   hold on;
   plot(saida(c,1:2:4), saida(c,2:2:4), '-', 'LineWidth', 1);
   plot(saida(c,5:2:8), saida(c,6:2:8), '-', 'LineWidth', 1);
   plot([saida(c,1) saida(c,5)], [saida(c,2) saida(c,6)], '-', 'LineWidth', 1);
   
   axis([-1, 1, -1, 1])
   pause(0.05);
end
