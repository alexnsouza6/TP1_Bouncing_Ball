
/** file: simulation.h
 ** brief: Simulation class (an interface)
 ** author: Andrea Vedaldi
 **/

#ifndef __simulation__
#define __simulation__

class Simulation
{
public:
    virtual void step(double dt) = 0 ;
    virtual void display() = 0 ;
    void run(Simulation & s, double dt){
        for (int i = 0 ; i < 100 ; ++i) { s.step(dt) ; s.display() ; }
    }
} ;

#endif /* defined(__simulation__) */
