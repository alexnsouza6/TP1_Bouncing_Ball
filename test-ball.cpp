/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>

int main(int argc, char** argv)
{
    Ball ball;
    const double dt = 1.0/30;
    ball.run(ball,dt);
  return 0 ;
}
