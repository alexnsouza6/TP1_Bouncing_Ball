/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

Ball::Ball(double newx, double newy){ /*Overload método Construtivo com a inicialização da posição desejada */
    
    if (xmin + r <= newx && newx <= xmax - r) {
        x = newx;
    } else{
        std:: cout << "Posição Inválida no eixo x" <<std::endl;
    }

    if (ymin + r <= newy && newy <= ymax - r) {
        y = newy;
    } else {
        std:: cout << "Posição Inválida no eixo y" <<std::endl;
    }

}

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}

void Ball::display()
{
  std::cout<<x<<" "<<y<<std::endl ;
}

/*Seção 3.1*/
void Ball::set_valuex(double newx) //Altera o valor da coordenada x
{
    if (xmin + r <= newx && newx <= xmax - r) {
      x = newx;
    } else{
      std:: cout << "Posição Inválida no eixo x" <<std::endl;
    }

}

void Ball::set_valeuy(double newy) //Altera o valor da coordenada y
{
    if (ymin + r <= newy && newy <= ymax - r) {
      y = newy;
    } else {
      std:: cout << "Posição Inválida no eixo y" <<std::endl;
    }
}

double Ball::get_valuex() //Verifica a posição da Bola em relação ao eixo x
{
    return x;
}
double Ball:: get_valuey() //Verifica a posição da Bola em relação ao eixo y
{
    return y;
}
