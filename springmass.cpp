/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

#include <iostream>
using namespace std;
/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
  double energy = 0 ;
  double e_cinetica = 0;
  double e_potencial = 0;
  double velocity1;

  velocity1 = velocity.norm();
  
  e_cinetica = mass*velocity1/2.0;  
  e_potencial = mass*gravity*(position.y - radius);
  energy = e_cinetica + e_potencial;

  return energy ;
}

void Mass::step(double dt)
{
  Vector2 force;
  double aceleracaox, aceleracaoy;
  double yp,xp;

  force = getForce();
  aceleracaox = force.x/mass;
  aceleracaoy = force.y/mass;
  xp = position.x + velocity.x*dt + (aceleracaox*dt*dt)/2.0;
  yp = position.y + velocity.y*dt + (aceleracaoy*dt*dt)/2.0;
  if (xmin + radius <= xp && xp <= xmax - radius) {
      position.x = xp;
    velocity.x = velocity.x + aceleracaox*dt;
  } else {
      velocity.x = -velocity.x ;
  }
    if (ymin + radius <= yp && yp <= ymax - radius) {
      position.y = yp ;
    velocity.y = velocity.y + aceleracaoy*dt;
    } else {
        velocity.y = -velocity.y ;
    }
}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping)
: mass1(mass1), mass2(mass2),
naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1 ;
}

Mass * Spring::getMass2() const
{
  return mass2 ;
}

Vector2 Spring::getForce() const
{
  Vector2 F,u,unit;
  double l = getLength();
  double force_mod,v_along, comp_mola;
  u = (mass2->getPosition() - mass1->getPosition());
  u.x-= 2*mass1->getRadius();
  u.y-= 2*mass1->getRadius();
  unit = u/u.norm();
  v_along  = dot(mass2->getVelocity() - mass1->getVelocity(),unit);
  force_mod = (naturalLength- u.norm())*stiffness + v_along*damping;
  F = force_mod*unit;
  return F ;
}

double Spring::getLength() const
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm() ;
}

double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity)
: gravity(gravity)
{ }

void SpringMass::display()
{
  int i = 0;
  Mass *m1, *m2;
  Vector2 v1;
  double energy = getEnergy();
  while(i <  (int)masses.size()){
    v1 = masses[i].getPosition();
    cout << v1.x << " " << v1.y << " " << endl;
    i++;  
  }
  i = 0;
  while(i < springs.size()){
    m1 = springs[i].getMass1();
    m2 = springs[i].getMass2();
    i++;  
  }
  cout << energy << endl; 
}

double SpringMass::getEnergy() const
{
  double energy = 0 ;
  int i=0;
  while(i < masses.size()){
    energy += masses[i].getEnergy(EARTH_GRAVITY);
    i++;  
  }
  i = 0;
  while(i < springs.size()){
    energy += springs[i].getEnergy();
    i++;  
  }

  return energy ;
}

void SpringMass::step(double dt)
{
  int i=0;
  Vector2 force;
  Vector2 g(0,-EARTH_GRAVITY) ; // Vetor da gravidade, i.e.,
  // aceleracao apontando para baixo.
  
  while(i < masses.size()){
    masses[i].setForce(g * masses[i].getMass());
    i++;  
  }
  i = 0;
  while(i < springs.size()){
    force = springs[i].getForce();  
    springs[i].getMass1()->addForce(-1* force) ;
    springs[i].getMass2()->addForce(+1* force) ;
    i++;
  }
  i = 0;
  while(i < masses.size()){
    masses[i].step(dt);
    i++;
  }
}

void SpringMass:: pushMass(Mass mass)
{
  masses.push_back(mass);
}

void SpringMass:: pushSpring(int a, int b, double damping, double naturalLength, double stiffness)
{
  Spring spring(&masses[a],&masses[b],naturalLength,stiffness,damping);
  springs.push_back(spring);
}
