# Bouncing Ball
Bouncing Ball é um experimento no qual observa-se o comportamento de uma bola, que é solta de uma certa altura. Partindo do repouso, essa bola executa movimentos que proporcionam a análise das energias potencial e cinética que estão envolvidas no processo tão bem quanto a análise do posicionamento e a velocidade com a qual a bola se move. 

# O Projeto
O projeto consiste em simular de maneira dinâmica o movimento de uma bola, sob o efeito da gravidade, que está contida em uma caixa [-1,1] x [-1,1]. 

# Requisições
O projeto se extende a utilização da linguagem C++. Para isso é necessária a utilização do compilador desta linguagem: g++, que é um dos _compiler-drivers_ do [GNU], uma coleção de compiladores.

# Compilação
Para a compilação dos arquivos contidos no projeto TP1_Bouncing_Ball é necessário ativar o console e utilizar o comando `g++ ball.cpp test-ball.cpp -o nome_do_objeto` no terminal.

# Módulos
O projeto é divido em apenas 4 módulos:

    1.Simulation.h
Esse módulo é responsável pela implementação da interface do simulador, que cria três funções virtuais, as quais são herdadas pela classe Ball para executar o experimento da Bouncing Ball.

	2.Ball.h
A interface da Bola está contida nesse módulo, uma vez que são mostrados todos os atributos da classe, que estão protegidos e todos os métodos, que são públicos.

	3.Ball.cpp
O módulo Ball.cpp contêm a implementação dos métodos presentes no módulo Ball.h

	4.test-ball.cpp
O módulo de teste instancia a classe Ball e põe ela à prova, chamando os métodos para que o experimento seja realizado.


# Class Diagram
![Diagram](Class_Diagram_TP1.png)

# Saídas do Programa
A bola inicia na posição (0,0) com as velocidades vx = 0,3 e vy = -0,1. Sendo assim, o experimento é iniciado e os cálculos da posição e da velocidade da bola são feitos pelo método **step()**. 

A partir disso, o resultado obtido da velocidade é usado como parâmetro de comparação com o valor do tamanho da caixa na direção X somado ao tamanho da bola, enquanto o valor da posição é utilizado como parâmetro de comparação com o valor do tamanho da caixa na direção Y somado ao tamanho da bola.

Por fim, essas comparações resultam na reconfiguração no espaço da bola dentro da caixa e assim, a coordenada da mesma é printada por meio do método **display()**.
A saída gerada pelo programa, sem modificações no modelo inicial (i.e atributos definidos pela função **Ball()** é:
	
	0.01 -0.00877778
	0.02 -0.0284444
	0.03 -0.059
	0.04 -0.100444
	0.05 -0.152778
	0.06 -0.216
	0.07 -0.290111
	0.08 -0.375111
	0.09 -0.471
	0.1 -0.577778
	0.11 -0.695444
	0.12 -0.824
	0.13 -0.824
	0.14 -0.695444
	0.15 -0.577778
	0.16 -0.471
	0.17 -0.375111
	0.18 -0.290111
	0.19 -0.216
	0.2 -0.152778
	0.21 -0.100444
	0.22 -0.059
	0.23 -0.0284444
	0.24 -0.00877778
	0.25 -8.1532e-17
	0.26 -0.00211111
	0.27 -0.0151111
	0.28 -0.039
	0.29 -0.0737778
	0.3 -0.119444
	0.31 -0.176
	0.32 -0.243444
	0.33 -0.321778
	0.34 -0.411
	0.35 -0.511111
	0.36 -0.622111
	0.37 -0.744
	0.38 -0.876778
	0.39 -0.876778
	0.4 -0.744
	0.41 -0.622111
	0.42 -0.511111
	0.43 -0.411
	0.44 -0.321778
	0.45 -0.243444
	0.46 -0.176
	0.47 -0.119444
	0.48 -0.0737778
	0.49 -0.039
	0.5 -0.0151111
	0.51 -0.00211111
	0.52 -4.90059e-16
	0.53 -0.00877778
	0.54 -0.0284444
	0.55 -0.059
	0.56 -0.100444
	0.57 -0.152778
	0.58 -0.216
	0.59 -0.290111
	0.6 -0.375111
	0.61 -0.471
	0.62 -0.577778
	0.63 -0.695444
	0.64 -0.824
	0.65 -0.824
	0.66 -0.695444
	0.67 -0.577778
	0.68 -0.471
	0.69 -0.375111
	0.7 -0.290111
	0.71 -0.216
	0.72 -0.152778
	0.73 -0.100444
	0.74 -0.059
	0.75 -0.0284444
	0.76 -0.00877778
	0.77 -7.47666e-16
	0.78 -0.00211111
	0.79 -0.0151111
	0.8 -0.039
	0.81 -0.0737778
	0.82 -0.119444
	0.83 -0.176
	0.84 -0.243444
	0.85 -0.321778
	0.86 -0.411
	0.87 -0.511111
	0.88 -0.622111
	0.89 -0.744
	0.89 -0.876778
	0.88 -0.876778
	0.87 -0.744
	0.86 -0.622111
	0.85 -0.511111
	0.84 -0.411
	0.83 -0.321778
	0.82 -0.243444
	0.81 -0.176
	0.8 -0.119444
	0.79 -0.0737778

A partir desses dados, é possível plotar um gráfico tal qual:

![Grafico](bouncing_ball.png)

O gráfico representa o movimento da bola dentro da caixa durante 100 iterações. O gráfico foi gerado utilizando o software [gnuplot].
    
   
   
# SpringMass
O sistema de massa mola consiste em duas massas, interconectadas por uma mola, que se movimentam no plano XY de acordo com a movimentação das massas, que são orientadas a partir do ganho ou perda de energia potencial e cinética, além da compressão e expansão da mola.

# Compilação
A compilação dos módulos `test-springmass.cpp, springmass.cpp e springmass.h` é dada via linha de comando utilizando o seguinte comando terminal `g++ springmass.cpp test-springmass.cpp -o nome_do_objeto`

# Módulos
O projeto é divido em apenas 3 módulos:

    1.Springmass.h
Esse módulo é responsável pela implementação da interface das massas, da mola e do sistema massa-mola, além da definição da classe Vector2 tão bem quanto o overload dos operadores referentes à operações entre vetores.

	2.Springmass.cpp
A interface da Mola, da Massa, do sistema Massa-Mola e da classe Vector2 estão contidas nesse módulo, uma vez que são mostrados todos os atributos das respectivas classes, além dos métodos que compõe as mesmas.

	3.test-springmass.cpp
O módulo test-springmass.cpp contêm a inicialização de todos os parâmetros das classes principais (i.e Mass, Spring e Springmass) além da simulação do sistema com duas massas e uma mola.

# Diagrama
![Diagram1](Diagram_SpringMass_System.png)

# Saída
    O módulo test-springmass.cpp chama o método *display()* e, assim, imprime na tela, para cada posição nova, as massas dos corpos ligados à mola, além da energia do sistema. Abaixo, seguem as primeiras linhas de saída do código.
    Ex:
    Massa: número_da_Massa
    M0_Position_X  M0_Position_y
    M1_Position_x  M1_Position_y
    System_Energy_Value
    
    Massa: 1
    0.501667 -0.00545556 
    -0.00844085
    Massa: 0
    -0.496667 -0.0218222 
    Massa: 1
    0.503333 -0.0218222 
    -0.00824076
    Massa: 0
    -0.495 -0.0491 
    Massa: 1
    0.505 -0.0491 
    -0.0186926
    Massa: 0
    -0.493333 -0.0872889 
    Massa: 1
    0.506667 -0.0872889 
    -0.0398433

# Apuração Gráfica
A partir da criação das classes **Ball** e **SprigMass**, tem-se que o objetivo, por hora, é tornar a observação desses sistemas possível graficamente por meio das bibliotecas OpenGL e GLUT.
	
# Dependências
Para simular o sistema graficamente, é necessário o uso das bibliotecas já mencionadas na seção acima:
    1. [OpenGL]
    2. [GLUT]	
A biblioteca OpenGL pode ser adquirida no sistema Ubunto a partir do comando `sudo apt-get install freeglut3 freeglut3-dev` no terminal.

# Como Compilar
A compilação das simulações de Ball e SpringMass são dadas de formas diferentes devido a relação entre as bibliotecas utilizadas na simulação gráfica e o sistema Operacional OS do Mac (utilizado na elaboração deste projeto).
Sendo assim, tem-se que para compilar a simulação gráfica de Ball, tal linha de comando deve ser executada na pasta do experimento:
    `g++ graphics.cpp ball.cpp test-ball-graphics.cpp -framework OpenGL -framework CoreFoundation -framework Carbon -framework GLUT -o teste -Wno-deprecated-declarations`.
Além disso, para compilar a simulação gráficas de SpringMass, a linha de comando:
    `g++ graphics.cpp springmass.cpp test-springmass-graphics.cpp -framework OpenGL -framework CoreFoundation -framework Carbon -framework GLUT -o teste -Wno-deprecated-declarations` deve ser executada na pasta que contém o projeto.
*obs*: As flags `-framework OpenGL -framework CoreFoundation -framework Carbon -framework GLUT -o teste -Wno-deprecated-declarations` foram adicionadas para promover a compilação em sistemas OS.
   
# Arquivos Referentes a Parte Gráfica
Para a parte gráfica, foram adicionados 5 arquivos:
    1. graphics.h: Define as classes que serão utilizadas para a simulação gráfica, além de fazer as inclusões das bibliotecas que serão utilizadas
    2. graphics.cpp: Esse módulo contém a interface de todos os métodos
    3. test-ball-graphics.cpp: Módulo responsável por representar graficamente a simulação da primeira parte do projeto referente a classe **Ball**
    4. test-springmass-graphics.cpp: Módulo responsável por representar graficamente a simulação da segunda parte do projeto referente a classe **SpringMass**
    
# Diagram Final
![Diagram2](Diagram_Graphics.png)

# Screenshots
 - Screenshots do funcionamento do sistema da classe **Ball**:
    - ![Scs1Ball](Ball_graphic.png) ![Scs2Ball](Ball_graphic2.png)
 - Screenshots do funcionamento do sistema da classe **SpringMass**
    - ![Scs1SpringMass](SpringMass_graphics.png) ![Scs2SpringMass](SpringMass_graphics2.png)
    
   [gnuplot]: <http://www.gnuplot.info>
   [GNU]: <https://gcc.gnu.org/>
   [OpenGL]: <http://www.opengl-tutorial.org/>
   [GLUT]: <https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/>